import React, { Component } from 'react'

const initial = {
    Name: '',
    Email: '',
    Phonenumber: '',
    Address: '',
    Birthdate: '',
    Age: '' ,
};
 class FormComponent extends Component {
    constructor(props){
      super(props)
      this.state = initial
      this.handleChange = this.handleChange.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
    }

handleSubmit(event){
    const {  Name,Email,Phonenumber, Address,Birthdate, Age } = this.state
   
    event.preventDefault()
    const record = this.state.records;
    record.push({Name,Email,Phonenumber, Address,Birthdate, Age})
    this.setState({records: record})
      
  }

  handleChange(event){
    this.setState({
      [event.target.name] : event.target.value
    })
  }

  render() {

     
  return(
    <>
        <div className="container">
            <form className="main-container" action=""onSubmit={(e) => this.handleSubmit(e)} >
                <h1>Registartion Form</h1>
                <div className="firstname" id="input">
                    <label htmlFor="firstname">Name :-</label><br/>
                    <input type="text" name="Name" placeholder="Enter Your Name"
                    value={this.state.Name}
                    onChange={this.handleChange}
                    autoComplete="off" ></input>
                </div>
                <div className="Email" id="input">
                    <label htmlFor="Email">Email Id :-</label><br/>
                    <input type="text" name="Email" placeholder="Enter your Email Id"
                    value={this.state.Email}
                    onChange={this.handleChange}
                    ></input>
                </div>
                <div className="Phone Number" id="input">
                    <label htmlFor="phone number">Phone Number :-</label><br/>
                    <input type="text" name="Phonenumber" placeholder="Enter Your Number"
                    value={this.state.Phonenumber}
                    onChange={this.handleChange}
                    ></input>
                </div>
                <div className="Address" id="input">
                    <label htmlFor="Address">Address :-</label><br/>
                    <textarea name="Address" style={{ margin: '10px', width: '300px', height: '80px' }} type="text" placeholder="Enter Your Address"
                    value={this.state.Address}
                    onChange={this.handleChange}
                    ></textarea>
                </div>
                <div className="Birthdate" id="input">
                    <label htmlFor="Birthdate">Birthdate :-</label><br/>
                    <input type="date" name="Birthdate" placeholder="Enter your BOD"
                    value={this.state.Birthdate}
                    onChange={this.handleChange}
                    ></input>
                </div>
                <div className="Age" id="input">
                    <label htmlFor="Age">Age :-</label><br/>
                    <input type="text" name="Age" placeholder="Enter Your Age"
                    value={this.state.Age}
                    onChange={this.handleChange}
                    ></input>
                </div>
                <button type="submit" className="btn" >Submit</button>
            </form>
        </div>

       <div> 
        {this.state.records.map((value, index) => {
            return (
                <tr key={index}>
                <td>{value.Name}</td>
                <td>{value.Email}</td>
                <td>{value.Phonenumber}</td>
                <td>{value.Address}</td>
                <td>{value.Birthdate}</td>
                <td>{value.Age}</td>
                </tr>
                
            )})
        }
        </div>
    </>
  )}}
export default FormComponent